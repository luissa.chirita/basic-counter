import { connect } from "react-redux";
import React, { Component, useState, useContext } from 'react';
import { context } from "./App.js";


// --------- Using Context ---------
export default function Counter() {
    const [count, setCount] = useContext(context);
    return (
        <div>
            <p>This is our current count: {count}</p>
            <button onClick={() => setCount(count + 1)}>Increment</button>
            <button onClick={() => setCount(count - 1)}>Decrement</button>
            <button onClick={() => setCount(0)}>Reset</button>
        </div>
    );
}

// --------- Using Redux ---------
/*
class Counter extends Component {
    increment = () => {
       this.props.dispatch({type: "INCREMENT"});
    };

    decrement = () => {
        this.props.dispatch({type: "DECREMENT"});
    };

    reset = () => {
        this.props.dispatch({type: "RESET"});
    };

    render() {
        return (
            <div className="counter">
                <p>This is our current count: {this.props.count}</p>
                <button onClick={this.increment}>Increment</button>
                <button onClick={this.decrement}>Decrement</button>
                <button onClick={this.reset}>Reset</button>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    count: state.count
});

export default connect(mapStateToProps)(Counter);
*/
