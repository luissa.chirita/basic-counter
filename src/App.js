import React, { Component, useState, createContext} from "react";
import ReactDOM from "react-dom";
import { createStore } from "redux";
import { Provider } from "react-redux";
import Counter from "./Counter";


// --------- Using Context ---------
const context = createContext();
export default function App() {
	const [count, setCount] = useState(0);

	return (
		<div className="appStyle">
			<context.Provider value={[count, setCount]}>
				<Counter />
			</context.Provider>
		</div>
	);
}

ReactDOM.render(<App />, document.getElementById('root'));
export { context };


// --------- Using Redux ---------
/*
const initialState = {
	count: 0
};

function reducer(state = initialState, action) {
	switch(action.type)  {
		case "INCREMENT":
		return {
			count: state.count + 1
		};
		case "DECREMENT":
		return {
			count: state.count - 1
		};
		case "RESET":
		return {
			count: 0
		};
		default:
		return state;
  }
}

const store = createStore(reducer);

export default function App() {
	return (
		<Provider store={store}>
			<Counter />
		</Provider>
	);
}

ReactDOM.render(<App />, document.getElementById('root'));
*/


// --------- Basic version ---------
/*
function Counter() {
	const [count, setCount] = useState(0);

	return(
		<div className="appStyle">
		<p>This is our current count: {count}</p>
		<button onClick={() => setCount(count + 1)}>Increment</button>
		<button onClick={() => setCount(count - 1)}>Decrement</button>
		<button onClick={() => setCount(0)}>Reset</button>
		</div>
	);
}

export default function App() {
	return (
		<div>
			<h1>Basic Counter</h1>
		</div>
	);
}

ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(<Counter />, document.getElementById('root'));
*/